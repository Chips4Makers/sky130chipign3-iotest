# SPDX-License-Identifier: GPL-2.0-or-later
from typing import Tuple, Dict, cast

from pdkmaster.technology import geometry as _geo, primitive as _prm
from pdkmaster.design import layout as _lay, library as _lbry

from c4m.pdk import sky130


__all__ = ["Core"]


def _find_sig(*, io_list, sig_name) -> Tuple[str, int]:
    def spec2sig(spec):
        sig_name = spec[1]
        if isinstance(sig_name, tuple):
            return sig_name[0]
        else:
            return sig_name

    for side, io_specs in io_list.items():
        io_sigs = tuple(map(spec2sig, io_specs))
        try:
            idx = io_sigs.index(sig_name)
        except ValueError:
            continue
        else:
            return (side, idx)
    else:
        raise ValueError(f"sig_name '{sig_name}' not in io_list")


class Core(_lbry._Cell[_lbry.Library]):
    def __init__(self, *,
        lib: _lbry.Library, io_nets, bb: _geo.Rect,
        io_list, io_lays: Dict[str, Tuple[_lay._Layout, ...]],
    ):
        super().__init__(lib=lib, name="Core")
        tech = lib.tech
        prims = tech.primitives

        stdcells = sky130.stdcelllib.cells

        mem_fab = sky130.Sky130SP6TFactory(lib=self.lib)

        nwm = cast(_prm.Well, prims.nwm)
        li = cast(_prm.MetalWire, prims.li)
        lipin = cast(_prm.Marker, prims["li.pin"])
        mcon = cast(_prm.Via, prims.mcon)
        m1 = cast(_prm.MetalWire, prims.m1)
        m1pin = cast(_prm.Marker, prims["m1.pin"])
        via = cast(_prm.Via, prims.via)
        m2 = cast(_prm.MetalWire, prims.m2)
        m2pin = cast(_prm.Marker, prims["m2.pin"])
        via2 = cast(_prm.Via, prims.via2)
        m3 = cast(_prm.MetalWire, prims.m3)
        m3pin = cast(_prm.Marker, prims["m3.pin"])
        bnd = cast(_prm.Marker, prims.prBoundary)

        zero_cell = stdcells.zero_x1
        zero_nets = zero_cell.circuit.nets
        _zero_zerolipinbb = zero_cell.layout.bounds(
            mask=lipin.mask, net=zero_nets.zero, depth=1,
        )
        _zero_bb = zero_cell.layout.boundary
        assert _zero_bb is not None

        one_cell = stdcells.one_x1
        _one_bb = one_cell.layout.boundary
        assert _one_bb is not None

        buf_cell = stdcells.buf_x2
        buf_nets = buf_cell.circuit.nets
        _buf_ilipinbb = buf_cell.layout.bounds(mask=lipin.mask, net=buf_nets.i, depth=1)
        _buf_bb = buf_cell.layout.boundary
        assert _buf_bb is not None

        tie_cell = stdcells.tie_diff_w4
        _tie_bb = tie_cell.layout.boundary
        assert _tie_bb is not None

        sram_cell = mem_fab.block(words=512, word_size=8, we_size=1)
        self.a_bits = sram_cell.a_bits
        self.word_size = sram_cell.word_size

        ckt = self.new_circuit()
        layouter = self.new_circuitlayouter()
        layout = layouter.layout


        # DC nets for logic
        dvss = ckt.new_net(name="dvss", external=False)
        dvdd = ckt.new_net(name="dvdd", external=False)


        # Place the SRAM
        #
        # instantiate
        sram = ckt.instantiate(sram_cell, name="sram")

        # place
        _sram_lay = layouter.inst_layout(inst=sram)
        _sram_bb = _sram_lay.boundary
        assert _sram_bb is not None

        x = sky130.tech.on_grid(bb.center.x - _sram_bb.center.x)
        y = bb.top - 150.0 - _sram_bb.top
        sram_lay = layouter.place(_sram_lay, x=x, y=y)
        sram_bb = sram_lay.boundary
        assert sram_bb is not None


        # Make rows to place standard cells in
        #
        dbound = 4.0
        # left row
        rot_leftrow = _geo.Rotation.R90
        _tie_rotbb = rot_leftrow*_tie_bb

        x_leftrow = bb.left + dbound - _tie_rotbb.left

        inst = ckt.instantiate(tie_cell, name="lltie")
        dvss.childports += inst.ports.vss
        dvdd.childports += inst.ports.vdd

        y_tie = bb.bottom + dbound - _tie_rotbb.bottom
        lay = layouter.place(inst, x=x_leftrow, y=y_tie, rotation=rot_leftrow)
        nwmbb1 = lay.bounds(mask=nwm.mask)
        lipindvssbb1 = lay.bounds(mask=lipin.mask, net=dvss, depth=1)
        lipindvddbb1 = lay.bounds(mask=lipin.mask, net=dvdd, depth=1)

        inst = ckt.instantiate(tie_cell, name="ultie")
        dvss.childports += inst.ports.vss
        dvdd.childports += inst.ports.vdd

        y_tie = bb.top - dbound - _tie_rotbb.top
        lay = layouter.place(inst, x=x_leftrow, y=y_tie, rotation=rot_leftrow)
        nwmbb2 = lay.bounds(mask=nwm.mask)
        lipindvssbb2 = lay.bounds(mask=lipin.mask, net=dvss, depth=1)
        lipindvddbb2 = lay.bounds(mask=lipin.mask, net=dvdd, depth=1)

        shape = _geo.Rect.from_rect(rect=nwmbb1, top=nwmbb2.top)
        layouter.add_wire(net=dvdd, wire=nwm, shape=shape)

        shape = _geo.Rect.from_rect(rect=lipindvssbb1, top=lipindvssbb2.top)
        layouter.add_wire(net=dvss, wire=li, shape=shape)
        w = tech.on_grid(shape.width, mult=2, rounding="floor")
        h = tech.on_grid(shape.height, mult=2, rounding="floor")
        o = tech.on_grid(shape.center)
        lay = layouter.add_wire(
            net=dvss, wire=mcon, origin=o, bottom_width=w, bottom_height=h,
        )
        dvss_leftrowm1bb = lay.bounds(mask=m1.mask)

        shape = _geo.Rect.from_rect(rect=lipindvddbb1, top=lipindvddbb2.top)
        layouter.add_wire(net=dvdd, wire=li, shape=shape)
        w = tech.on_grid(shape.width, mult=2, rounding="floor")
        h = tech.on_grid(shape.height, mult=2, rounding="floor")
        o = tech.on_grid(shape.center)
        lay = layouter.add_wire(
            net=dvdd, wire=mcon, origin=o, bottom_width=w, bottom_height=h,
        )
        dvdd_leftrowm1bb = lay.bounds(mask=m1.mask)

        # connect m1 DC tracks to IO ring at the top
        # vss -> m3
        w = dvss_leftrowm1bb.width
        _l = layouter.wire_layout(
            net=dvss, wire=via,
            bottom_width=w, bottom_height=w, bottom_enclosure="tall",
            top_width=w, top_height=w, top_enclosure="tall",
        )
        _via_m1bb = _l.bounds(mask=m1.mask)
        x = dvss_leftrowm1bb.center.x
        y = dvss_leftrowm1bb.top - _via_m1bb.top
        l = layouter.place(_l, x=x, y=y)
        via_m2bb = l.bounds(mask=m2.mask)
        l = layouter.add_wire(
            net=dvss, wire=via2, origin=via_m2bb.center,
            bottom_width=w, bottom_height=w, bottom_enclosure="tall",
            top_width=w, top_height=w, top_enclosure="tall",
        )
        via2_m3bb = l.bounds(mask=m3.mask)
        shape = _geo.Rect.from_rect(rect=via2_m3bb, top=bb.top)
        layouter.add_wire(net=dvss, wire=m3, pin=m3pin, shape=shape)

        # vdd -> m2
        w = dvdd_leftrowm1bb.width
        _l = layouter.wire_layout(
            net=dvdd, wire=via,
            bottom_width=w, bottom_height=w, bottom_enclosure="tall",
            top_width=w, top_height=w, top_enclosure="tall",
        )
        _via_m1bb = _l.bounds(mask=m1.mask)
        x = dvdd_leftrowm1bb.center.x
        y = dvdd_leftrowm1bb.top - _via_m1bb.top
        l = layouter.place(_l, x=x, y=y)
        via_m2bb = l.bounds(mask=m2.mask)
        shape = _geo.Rect.from_rect(rect=via_m2bb, top=bb.top)
        layouter.add_wire(net=dvdd, wire=m2, pin=m2pin, shape=shape)

        # right row
        rot_rightrow = _geo.Rotation.R90
        _tie_rotbb = rot_rightrow*_tie_bb

        x_rightrow = bb.right - dbound - _tie_rotbb.right

        inst = ckt.instantiate(tie_cell, name="lrtie")
        dvss.childports += inst.ports.vss
        dvdd.childports += inst.ports.vdd

        y_tie = bb.bottom + dbound - _tie_rotbb.bottom
        lay = layouter.place(inst, x=x_rightrow, y=y_tie, rotation=rot_rightrow)
        nwmbb1 = lay.bounds(mask=nwm.mask)
        lipindvssbb1 = lay.bounds(mask=lipin.mask, net=dvss, depth=1)
        lipindvddbb1 = lay.bounds(mask=lipin.mask, net=dvdd, depth=1)

        inst = ckt.instantiate(tie_cell, name="urtie")
        dvss.childports += inst.ports.vss
        dvdd.childports += inst.ports.vdd

        y_tie = bb.top - dbound - _tie_rotbb.top
        lay = layouter.place(inst, x=x_rightrow, y=y_tie, rotation=rot_rightrow)
        nwmbb2 = lay.bounds(mask=nwm.mask)
        lipindvssbb2 = lay.bounds(mask=lipin.mask, net=dvss, depth=1)
        lipindvddbb2 = lay.bounds(mask=lipin.mask, net=dvdd, depth=1)

        shape = _geo.Rect.from_rect(rect=nwmbb1, top=nwmbb2.top)
        layouter.add_wire(net=dvdd, wire=nwm, shape=shape)

        shape = _geo.Rect.from_rect(rect=lipindvssbb1, top=lipindvssbb2.top)
        layouter.add_wire(net=dvss, wire=li, shape=shape)
        w = tech.on_grid(shape.width, mult=2, rounding="floor")
        h = tech.on_grid(shape.height, mult=2, rounding="floor")
        o = tech.on_grid(shape.center)
        lay = layouter.add_wire(
            net=dvss, wire=mcon, origin=o, bottom_width=w, bottom_height=h,
        )
        dvss_rightrowm1bb = lay.bounds(mask=m1.mask)

        shape = _geo.Rect.from_rect(rect=lipindvddbb1, top=lipindvddbb2.top)
        layouter.add_wire(net=dvdd, wire=li, shape=shape)
        w = tech.on_grid(shape.width, mult=2, rounding="floor")
        h = tech.on_grid(shape.height, mult=2, rounding="floor")
        o = tech.on_grid(shape.center)
        lay = layouter.add_wire(
            net=dvdd, wire=mcon, origin=o, bottom_width=w, bottom_height=h,
        )
        dvdd_rightrowm1bb = lay.bounds(mask=m1.mask)

        # connect m1 DC tracks to IO ring at the top
        # vss -> m3
        w = dvss_rightrowm1bb.width
        _l = layouter.wire_layout(
            net=dvss, wire=via,
            bottom_width=w, bottom_height=w, bottom_enclosure="tall",
            top_width=w, top_height=w, top_enclosure="tall",
        )
        _via_m1bb = _l.bounds(mask=m1.mask)
        x = dvss_rightrowm1bb.center.x
        y = dvss_rightrowm1bb.top - _via_m1bb.top
        l = layouter.place(_l, x=x, y=y)
        via_m2bb = l.bounds(mask=m2.mask)
        l = layouter.add_wire(
            net=dvss, wire=via2, origin=via_m2bb.center,
            bottom_width=w, bottom_height=w, bottom_enclosure="tall",
            top_width=w, top_height=w, top_enclosure="tall",
        )
        via2_m3bb = l.bounds(mask=m3.mask)
        shape = _geo.Rect.from_rect(rect=via2_m3bb, top=bb.top)
        layouter.add_wire(net=dvss, wire=m3, pin=m3pin, shape=shape)

        # vdd -> m2
        w = dvdd_rightrowm1bb.width
        _l = layouter.wire_layout(
            net=dvdd, wire=via,
            bottom_width=w, bottom_height=w, bottom_enclosure="tall",
            top_width=w, top_height=w, top_enclosure="tall",
        )
        _via_m1bb = _l.bounds(mask=m1.mask)
        x = dvdd_rightrowm1bb.center.x
        y = dvdd_rightrowm1bb.top - _via_m1bb.top
        l = layouter.place(_l, x=x, y=y)
        via_m2bb = l.bounds(mask=m2.mask)
        shape = _geo.Rect.from_rect(rect=via_m2bb, top=bb.top)
        layouter.add_wire(net=dvdd, wire=m2, pin=m2pin, shape=shape)

        # bottom row
        rot_botrow = _geo.Rotation.R0
        _tie_rotbb = rot_botrow*_tie_bb

        y_botrow = bb.bottom + dbound - _tie_rotbb.bottom

        inst = ckt.instantiate(tie_cell, name="blltie")
        dvss.childports += inst.ports.vss
        dvdd.childports += inst.ports.vdd

        x_tie = dvss_leftrowm1bb.right + dbound - _tie_rotbb.left
        lay = layouter.place(inst, x=x_tie, y=y_botrow, rotation=rot_botrow)
        nwmbb1 = lay.bounds(mask=nwm.mask)
        lipindvssbb1 = lay.bounds(mask=lipin.mask, net=dvss, depth=1)
        lipindvddbb1 = lay.bounds(mask=lipin.mask, net=dvdd, depth=1)

        inst = ckt.instantiate(tie_cell, name="blrtie")
        dvss.childports += inst.ports.vss
        dvdd.childports += inst.ports.vdd

        x_tie = dvdd_rightrowm1bb.left - dbound - _tie_rotbb.right
        lay = layouter.place(inst, x=x_tie, y=y_botrow, rotation=rot_botrow)
        nwmbb2 = lay.bounds(mask=nwm.mask)
        lipindvssbb2 = lay.bounds(mask=lipin.mask, net=dvss, depth=1)
        lipindvddbb2 = lay.bounds(mask=lipin.mask, net=dvdd, depth=1)

        shape = _geo.Rect.from_rect(rect=nwmbb1, right=nwmbb2.right)
        layouter.add_wire(net=dvdd, wire=nwm, shape=shape)

        shape = _geo.Rect.from_rect(rect=lipindvssbb1, right=lipindvssbb2.right)
        layouter.add_wire(net=dvss, wire=li, shape=shape)
        w = tech.on_grid(shape.width, mult=2, rounding="floor")
        h = tech.on_grid(shape.height, mult=2, rounding="floor")
        o = tech.on_grid(shape.center)
        lay = layouter.add_wire(
            net=dvss, wire=mcon, origin=o, bottom_width=w, bottom_height=h,
        )
        dvss_bottomrowm1bb = lay.bounds(mask=m1.mask)

        shape = _geo.Rect.from_rect(rect=lipindvddbb1, right=lipindvddbb2.right)
        layouter.add_wire(net=dvdd, wire=li, shape=shape)
        w = tech.on_grid(shape.width, mult=2, rounding="floor")
        h = tech.on_grid(shape.height, mult=2, rounding="floor")
        o = tech.on_grid(shape.center)
        lay = layouter.add_wire(
            net=dvdd, wire=mcon, origin=o, bottom_width=w, bottom_height=h,
        )
        dvdd_bottomrowm1bb = lay.bounds(mask=m1.mask)

        shape = _geo.Rect.from_rect(rect=dvss_bottomrowm1bb, left=dvss_leftrowm1bb.left)
        layouter.add_wire(net=dvss, wire=m1, shape=shape)

        w = dvss_rightrowm1bb.width
        h = dvss_bottomrowm1bb.height
        _l = layouter.wire_layout(
            net=dvss, wire=via,
            bottom_width=w, bottom_height=h, bottom_enclosure="wide",
            top_width=w, top_height=h, top_enclosure="wide",
        )
        _via_m1bb = _l.bounds(mask=m1.mask)
        x = dvss_bottomrowm1bb.right - _via_m1bb.right
        y = dvss_bottomrowm1bb.center.y
        l = layouter.place(_l, x=x, y=y)
        via_m2bb1 = l.bounds(mask=m2.mask)
        x = dvss_rightrowm1bb.left - _via_m1bb.left
        l = layouter.place(_l, x=x, y=y)
        via_m2bb2 = l.bounds(mask=m2.mask)

        shape = _geo.Rect.from_rect(rect=via_m2bb1, right=via_m2bb2.right)
        layouter.add_wire(net=dvss, wire=m2, shape=shape)

        # IO ring has ring for vss on metal3 touching the core
        # Add connection to that
        l = layouter.add_wire(
            net=dvss, wire=via2, origin=via_m2bb2.center,
            bottom_width=w, bottom_height=h, bottom_enclosure="wide",
            top_width=w, top_height=h, top_enclosure="wide",
        )
        via2_m3bb = l.bounds(mask=m3.mask)
        shape = _geo.Rect.from_rect(rect=via2_m3bb, right=bb.right)
        layouter.add_wire(net=dvss, wire=m3, pin=m3pin, shape=shape)

        _l = layouter.wire_layout(
            net=dvss, wire=via,
            bottom_width=w, bottom_height=h, bottom_enclosure="wide",
            top_width=w, top_height=h, top_enclosure="wide",
        )
        _via_m1bb = _l.bounds(mask=m1.mask)
        x = dvss_leftrowm1bb.left - _via_m1bb.left
        y = dvss_bottomrowm1bb.center.y
        l = layouter.place(_l, x=x, y=y)
        via_m2bb = l.bounds(mask=m2.mask)
        l = layouter.add_wire(
            net=dvss, wire=via2, origin=via_m2bb.center,
            bottom_width=w, bottom_height=h, bottom_enclosure="wide",
            top_width=w, top_height=h, top_enclosure="wide",
        )
        via2_m3bb = l.bounds(mask=m3.mask)
        shape = _geo.Rect.from_rect(rect=via2_m3bb, left=bb.left)
        layouter.add_wire(net=dvss, wire=m3, pin=m3pin, shape=shape)

        shape = _geo.Rect.from_rect(rect=dvdd_bottomrowm1bb, right=dvdd_rightrowm1bb.right)
        layouter.add_wire(net=dvdd, wire=m1, shape=shape)

        w = dvdd_leftrowm1bb.width
        h = dvdd_bottomrowm1bb.height
        _l = layouter.wire_layout(
            net=dvdd, wire=via,
            bottom_width=w, bottom_height=h, bottom_enclosure="wide",
            top_width=w, top_height=h, top_enclosure="wide",
        )
        _via_m1bb = _l.bounds(mask=m1.mask)
        x = dvdd_bottomrowm1bb.left - _via_m1bb.left
        y = dvdd_bottomrowm1bb.center.y
        l = layouter.place(_l, x=x, y=y)
        via_m2bb1 = l.bounds(mask=m2.mask)
        x = dvdd_leftrowm1bb.right - _via_m1bb.right
        l = layouter.place(_l, x=x, y=y)
        via_m2bb2 = l.bounds(mask=m2.mask)

        # The IO ring has a ring for vdd on m2, extend the rect to the IO ring
        shape = _geo.Rect.from_rect(rect=via_m2bb1, left=bb.left)
        layouter.add_wire(net=dvdd, wire=m2, pin=m2pin, shape=shape)

        # Connect to IO right on right
        _l = layouter.wire_layout(
            net=dvdd, wire=via,
            bottom_width=w, bottom_height=h, bottom_enclosure="wide",
            top_width=w, top_height=h, top_enclosure="wide",
        )
        _via_m1bb = _l.bounds(mask=m1.mask)
        x = dvdd_rightrowm1bb.right - _via_m1bb.right
        y = dvdd_bottomrowm1bb.center.y
        l = layouter.place(_l, x=x, y=y)
        via_m2bb = l.bounds(mask=m2.mask)
        shape = _geo.Rect.from_rect(rect=via_m2bb, right=bb.right)
        layouter.add_wire(net=dvdd, wire=m2, pin=m2pin, shape=shape)

        # below SRAM
        rot_midrow = _geo.Rotation.R0
        _tie_rotbb = rot_midrow*_tie_bb

        y_midrow = sram_bb.bottom - dbound - _tie_rotbb.top

        inst = ckt.instantiate(tie_cell, name="mltie")
        dvss.childports += inst.ports.vss
        dvdd.childports += inst.ports.vdd

        x_tie = sram_bb.left - _tie_rotbb.left
        lay = layouter.place(inst, x=x_tie, y=y_midrow, rotation=rot_midrow)
        nwmbb1 = lay.bounds(mask=nwm.mask)
        lipindvssbb1 = lay.bounds(mask=lipin.mask, net=dvss, depth=1)
        lipindvddbb1 = lay.bounds(mask=lipin.mask, net=dvdd, depth=1)

        inst = ckt.instantiate(tie_cell, name="mrtie")
        dvss.childports += inst.ports.vss
        dvdd.childports += inst.ports.vdd

        x_tie = sram_bb.right - _tie_rotbb.right
        lay = layouter.place(inst, x=x_tie, y=y_midrow, rotation=rot_midrow)
        nwmbb2 = lay.bounds(mask=nwm.mask)
        lipindvssbb2 = lay.bounds(mask=lipin.mask, net=dvss, depth=1)
        lipindvddbb2 = lay.bounds(mask=lipin.mask, net=dvdd, depth=1)

        shape = _geo.Rect.from_rect(rect=nwmbb1, right=nwmbb2.right)
        layouter.add_wire(net=dvdd, wire=nwm, shape=shape)

        shape = _geo.Rect.from_rect(rect=lipindvssbb1, right=lipindvssbb2.right)
        layouter.add_wire(net=dvss, wire=li, shape=shape)
        w = tech.on_grid(shape.width, mult=2, rounding="floor")
        h = tech.on_grid(shape.height, mult=2, rounding="floor")
        o = tech.on_grid(shape.center)
        lay = layouter.add_wire(
            net=dvss, wire=mcon, origin=o, bottom_width=w, bottom_height=h,
        )
        dvss_midrowm1bb = lay.bounds(mask=m1.mask)

        shape = _geo.Rect.from_rect(rect=lipindvddbb1, right=lipindvddbb2.right)
        layouter.add_wire(net=dvdd, wire=li, shape=shape)
        w = tech.on_grid(shape.width, mult=2, rounding="floor")
        h = tech.on_grid(shape.height, mult=2, rounding="floor")
        o = tech.on_grid(shape.center)
        lay = layouter.add_wire(
            net=dvdd, wire=mcon, origin=o, bottom_width=w, bottom_height=h,
        )
        dvdd_midrowm1bb = lay.bounds(mask=m1.mask)

        assert dvss_leftrowm1bb.center.x > dvdd_leftrowm1bb.center.x, "Internal error"
        assert dvss_rightrowm1bb.center.x > dvdd_rightrowm1bb.center.x, "Internal error"

        shape = _geo.Rect.from_rect(
            rect=dvss_midrowm1bb,
            left=dvss_leftrowm1bb.left, right=dvdd_rightrowm1bb.left - 1.0,
        )
        layouter.add_wire(net=dvss, wire=m1, shape=shape)

        w = shape.height
        _via_lay = layouter.wire_layout(
            net=dvss, wire=via, bottom_width=w, bottom_height=w,
        )
        _via_m1bb = _via_lay.bounds(mask=m1.mask)

        y_via = shape.center.y
        x_via = shape.right - _via_m1bb.right
        lay = layouter.place(_via_lay, x=x_via, y=y_via)
        m2bb1 = lay.bounds(mask=m2.mask)
        x_via = dvss_rightrowm1bb.center.x
        lay = layouter.place(_via_lay, x=x_via, y=y_via)
        m2bb2 = lay.bounds(mask=m2.mask)

        shape = _geo.Rect.from_rect(rect=m2bb1, right=m2bb2.right)
        layouter.add_wire(net=dvss, wire=m2, shape=shape)

        # IO ring has ring for vss on metal3 touching the core
        # Add connection to that
        l = layouter.add_wire(
            net=dvss, wire=via2, origin=m2bb2.center,
            bottom_width=w, bottom_height=h, bottom_enclosure="wide",
            top_width=w, top_height=h, top_enclosure="wide",
        )
        via2_m3bb = l.bounds(mask=m3.mask)
        shape = _geo.Rect.from_rect(rect=via2_m3bb, right=bb.right)
        layouter.add_wire(net=dvss, wire=m3, pin=m3pin, shape=shape)

        _l = layouter.wire_layout(
            net=dvss, wire=via,
            bottom_width=w, bottom_height=h, bottom_enclosure="wide",
            top_width=w, top_height=h, top_enclosure="wide",
        )
        _via_m1bb = _l.bounds(mask=m1.mask)

        x = dvss_leftrowm1bb.left - _via_m1bb.left
        y = dvss_midrowm1bb.center.y
        l = layouter.place(_l, x=x, y=y)
        via_m2bb = l.bounds(mask=m2.mask)
        l = layouter.add_wire(
            net=dvss, wire=via2, origin=via_m2bb.center,
            bottom_width=w, bottom_height=h, bottom_enclosure="wide",
            top_width=w, top_height=h, top_enclosure="wide",
        )
        via2_m3bb = l.bounds(mask=m3.mask)
        shape = _geo.Rect.from_rect(rect=via2_m3bb, left=bb.left)
        layouter.add_wire(net=dvss, wire=m3, pin=m3pin, shape=shape)

        shape = _geo.Rect.from_rect(
            rect=dvdd_midrowm1bb,
            left=(dvss_leftrowm1bb.right + 1.0), right=dvdd_rightrowm1bb.right,
        )
        layouter.add_wire(net=dvdd, wire=m1, shape=shape)

        w = shape.height
        _via_lay = layouter.wire_layout(
            net=dvdd, wire=via, bottom_width=w, bottom_height=w,
        )
        _via_m1bb = _via_lay.bounds(mask=m1.mask)

        y_via = shape.center.y
        x_via = shape.left - _via_m1bb.left
        lay = layouter.place(_via_lay, x=x_via, y=y_via)
        m2bb1 = lay.bounds(mask=m2.mask)
        x_via = dvdd_leftrowm1bb.center.x
        lay = layouter.place(_via_lay, x=x_via, y=y_via)
        m2bb2 = lay.bounds(mask=m2.mask)

        # The IO ring has a ring for vdd on m2, extend the rect to the IO ring
        shape = _geo.Rect.from_rect(rect=m2bb1, left=bb.left)
        layouter.add_wire(net=dvdd, wire=m2, pin=m2pin, shape=shape)

        # Connect to IO right on right
        _l = layouter.wire_layout(
            net=dvdd, wire=via,
            bottom_width=w, bottom_height=h, bottom_enclosure="wide",
            top_width=w, top_height=h, top_enclosure="wide",
        )
        _via_m1bb = _l.bounds(mask=m1.mask)
        x = dvdd_rightrowm1bb.right - _via_m1bb.right
        y = dvdd_midrowm1bb.center.y
        l = layouter.place(_l, x=x, y=y)
        via_m2bb = l.bounds(mask=m2.mask)
        shape = _geo.Rect.from_rect(rect=via_m2bb, right=bb.right)
        layouter.add_wire(net=dvdd, wire=m2, pin=m2pin, shape=shape)

        # Connect the SRAM signals
        #
        # vss
        iosig_name = "sram_vss"
        sig_name = "vss"
        side, idx = _find_sig(io_list=io_list, sig_name=iosig_name)
        assert side == "west"
        io_net = io_nets[iosig_name]
        io_lay = io_lays[side][idx]

        sram_port = sram.ports[sig_name]
        net = ckt.new_net(name=sig_name, external=True, childports=sram_port)
        toppin_bb = io_lay.bounds(net=io_net, mask=m1pin.mask, depth=1)
        bbs = tuple(ms.shape.bounds for ms in sram_lay.filter_polygons(
            net=net, mask=m2pin.mask, depth=1, split=True,
        ))
        left = max(bb.left for bb in bbs)
        top = max(bb.top for bb in bbs)
        bottom = min(bb.bottom for bb in bbs)
        w = 10.0
        x_via2 = left + w/2.0
        right = left + w

        for dc_bb in bbs:
            y_via2 = dc_bb.center.y
            layouter.add_wire(
                net=net, wire=via2, x=x_via2, y=y_via2,
                bottom_width=w, bottom_enclosure="wide",
                top_width=w, top_enclosure="wide",
            )

        shape = _geo.Rect(
            left=left, bottom=bottom, right=right, top=toppin_bb.center.y,
        )
        layouter.add_wire(net=net, wire=m3, shape=shape)

        l = layouter.add_wire(
            net=net, wire=via2, x=x_via2, y=toppin_bb.center.y,
            bottom_width=w, bottom_height=w, bottom_enclosure="wide",
            top_width=w, top_height=w, top_enclosure="wide",
        )
        via_m2bb1 = l.bounds(mask=m2.mask)

        _l_via = layouter.wire_layout(
            net=net, wire=via, columns=2,
            bottom_height=w, bottom_enclosure="wide",
            top_height=w,  top_enclosure="wide",
        )
        _via_m1bb = _l_via.bounds(mask=m1.mask)
        x_via = toppin_bb.right - _via_m1bb.left + 2*m2.min_space
        y_via = toppin_bb.center.y
        l_via = layouter.place(_l_via, x=x_via, y=y_via)
        via_m1bb = l_via.bounds(mask=m2.mask)
        via_m2bb2 = l_via.bounds(mask=m2.mask)

        shape = _geo.Rect.from_rect(rect=toppin_bb, right=via_m1bb.right)
        layouter.add_wire(net=net, wire=m1, shape=shape)
        shape = _geo.Rect.from_rect(rect=via_m2bb2, right=via_m2bb1.right)
        layouter.add_wire(net=net, wire=m2, shape=shape)

        # vdd
        iosig_name = "sram_vdd"
        sig_name = "vdd"
        side, idx = _find_sig(io_list=io_list, sig_name=iosig_name)
        assert side == "east"
        io_net = io_nets[iosig_name]
        io_lay = io_lays[side][idx]

        sram_port = sram.ports[sig_name]
        net = ckt.new_net(name=sig_name, external=True, childports=sram_port)
        toppin_bb = io_lay.bounds(net=io_net, mask=m1pin.mask, depth=1)
        bbs = tuple(ms.shape.bounds for ms in sram_lay.filter_polygons(
            net=net, mask=m2pin.mask, depth=1, split=True,
        ))
        right = min(bb.right for bb in bbs)
        top = max(bb.top for bb in bbs)
        bottom = min(bb.bottom for bb in bbs)
        w = 10.0
        x_via2 = right - w/2.0
        left = right - w

        for dc_bb in bbs:
            y_via2 = dc_bb.center.y
            layouter.add_wire(
                net=net, wire=via2, x=x_via2, y=y_via2,
                bottom_width=w, bottom_enclosure="wide",
                top_width=w, top_enclosure="wide",
            )

        shape = _geo.Rect(
            left=left, bottom=bottom, right=right, top=toppin_bb.center.y,
        )
        layouter.add_wire(net=net, wire=m3, shape=shape)

        l = layouter.add_wire(
            net=net, wire=via2, x=x_via2, y=toppin_bb.center.y,
            bottom_width=w, bottom_height=w, bottom_enclosure="wide",
            top_width=w, top_height=w, top_enclosure="wide",
        )
        via_m2bb1 = l.bounds(mask=m2.mask)

        _l_via = layouter.wire_layout(
            net=net, wire=via, columns=2,
            bottom_height=w, bottom_enclosure="wide",
            top_height=w, top_enclosure="wide",
        )
        _via_m1bb = _l_via.bounds(mask=m1.mask)
        x_via = toppin_bb.left - 2*m2.min_space - _via_m1bb.right
        y_via = toppin_bb.center.y
        l_via = layouter.place(_l_via, x=x_via, y=y_via)
        via_m1bb = l_via.bounds(mask=m1.mask)
        via_m2bb2 = l_via.bounds(mask=m2.mask)

        shape = _geo.Rect.from_rect(rect=toppin_bb, left=via_m1bb.left)
        layouter.add_wire(net=net, wire=m1, shape=shape)
        shape = _geo.Rect.from_rect(rect=via_m2bb2, left=via_m2bb1.left)
        layouter.add_wire(net=net, wire=m2, shape=shape)

        conn_width = 8.0
        # connect the input signals
        a_col = 0
        for sig_name in (
            *(f"a[{a_bit}]" for a_bit in range(self.a_bits)),
            "clk", "we[0]",
            *(f"d[{d_bit}]" for d_bit in range(self.word_size))
        ):
            side, idx = _find_sig(io_list=io_list, sig_name=sig_name)
            assert side != "north"
            io_net = io_nets[sig_name]
            io_type = io_list[side][idx][0]
            io_lay = io_lays[side][idx]

            assert io_type == "in"

            sram_port = sram.ports[sig_name]

            # instantiate cells
            buf = ckt.instantiate(buf_cell, name="buf_{sig_name}")
            tie = ckt.instantiate(tie_cell, name=f"tie_{sig_name}")

            # create nets
            pin_net = ckt.new_net(name=f"io_{sig_name}", external=True, childports=buf.ports.i)
            sig_net = ckt.new_net(name=sig_name, external=False, childports=(
                buf.ports.q, sram_port,
            ))

            # get bbs; SRAM m2 pin to m3 pin
            toppin_bb = io_lay.bounds(mask=m1pin.mask, net=io_net, depth=1)
            if sig_name.startswith("a["):
                # Connect signal up to m2
                sram_m1pinbb = sram_lay.bounds(mask=m1pin.mask, net=sig_net, depth=1)

                right = sram_bb.left - (2*a_col + 1)*conn_width
                left = right - 1
                a_col += 1

                x_via = right - w/2.0
                y_via = tech.on_grid(sram_m1pinbb.center.y)
                via_lay = layouter.add_wire(
                    net=sig_net, wire=via, x=x_via, y=y_via,
                    bottom_width=w, bottom_enclosure="wide",
                    top_width=w, top_enclosure="wide",
                )
                via_m1bb = via_lay.bounds(mask=m1.mask)
                sram_m2pinbb = via_lay.bounds(mask=m2.mask)

                shape = _geo.Rect.from_rect(rect=sram_m1pinbb, left=via_m1bb.left)
                layouter.add_wire(net=sig_net, wire=m1, shape=shape)
            else:
                sram_m2pinbb = sram_lay.bounds(mask=m2pin.mask, net=sig_net, depth=1)

            rot = {
                "west": rot_leftrow,
                "east": rot_rightrow,
                "south": rot_botrow,
            }[side]
            x_row = {
                "west": x_leftrow,
                "east": x_rightrow,
                "south": None,
            }[side]
            y_row = {
                "west": None,
                "east": None,
                "south": y_botrow,
            }[side]

            # place buf
            _buf_rotilipinbb = rot*_buf_ilipinbb
            if side in ("west", "east"):
                x_buf = x_row
                y_buf = tech.on_grid(
                    toppin_bb.top - _buf_rotilipinbb.bottom,
                    mult=2,
                )
            else:
                assert side == "south"
                x_buf = tech.on_grid(
                    toppin_bb.left - _buf_rotilipinbb.right,
                )
                y_buf = y_row
            buf_lay = layouter.place(buf, x=x_buf, y=y_buf, rotation=rot)
            pinbuf_lipinbb = buf_lay.bounds(mask=lipin.mask, net=pin_net, depth=1)
            sigbuf_lipinbb = buf_lay.bounds(mask=lipin.mask, net=sig_net, depth=1)
            buf_bb = buf_lay.boundary
            assert buf_bb is not None

            # place tie cell
            _tie_rotbb = rot*_tie_bb
            if side == "west":
                x_tie = x_row
                y_tie = buf_bb.top - _tie_rotbb.bottom
            elif side == "east":
                x_tie = x_row
                y_tie = buf_bb.bottom - _tie_rotbb.top
            else:
                assert side == "south"
                x_tie = buf_bb.right - _tie_rotbb.left
                y_tie=y_row
            layouter.place(tie, x=x_tie, y=y_tie, rotation=rot)

            # connect pin_net
            if side in ("east", "west"):
                w = tech.on_grid(pinbuf_lipinbb.width, mult=2, rounding="floor")
                o_via = tech.on_grid(pinbuf_lipinbb.center)
                layouter.add_wire(
                    net=pin_net, wire=mcon, origin=o_via,
                    bottom_width=w, bottom_enclosure="wide",
                    top_width=w, top_enclosure="wide",
                )
                via_lay = layouter.add_wire(
                    net=pin_net, wire=via, origin=o_via,
                    bottom_width=w, bottom_enclosure="wide",
                    top_width=w, top_enclosure="wide",
                )
                via_m2bb = via_lay.bounds(mask=m2.mask)

                if side == "west":
                    shape = _geo.Rect.from_rect(
                        rect=via_m2bb, left=(toppin_bb.right + 2*m2.min_space),
                    )
                else:
                    shape = _geo.Rect.from_rect(
                        rect=via_m2bb, right=(toppin_bb.left - 2*m2.min_space),
                    )
                layouter.add_wire(net=pin_net, wire=m2, shape=shape)
                _l = layouter.wire_layout(
                    net=pin_net, wire=via, columns=2,
                    bottom_enclosure="wide", top_enclosure="wide",
                )
                _via_m2bb = _l.bounds(mask=m2.mask)
                if side == "west":
                    x = shape.left - _via_m2bb.left
                else:
                    x = shape.right - _via_m2bb.right
                y = shape.bottom - _via_m2bb.top
                l = layouter.place(_l, x=x, y=y)
                via_m1bb = l.bounds(mask=m1.mask)

                if side == "west":
                    shape = _geo.Rect.from_rect(rect=toppin_bb, right=via_m1bb.right)
                else:
                    shape = _geo.Rect.from_rect(rect=toppin_bb, left=via_m1bb.left)
                layouter.add_wire(net=pin_net, wire=m1, pin=m1pin, shape=shape)
            else:
                assert side == "south"
                h = tech.on_grid(pinbuf_lipinbb.height, mult=2, rounding="floor")
                o_via = tech.on_grid(pinbuf_lipinbb.center)
                layouter.add_wire(
                    net=pin_net, wire=mcon, origin=o_via,
                    bottom_height=h, bottom_enclosure="tall",
                    top_height=h, top_enclosure="tall",
                )
                via_lay = layouter.add_wire(
                    net=pin_net, wire=via, origin=o_via,
                    bottom_height=h, bottom_enclosure="tall",
                    top_height=h, top_enclosure="tall",
                )
                via_m2bb = via_lay.bounds(mask=m2.mask)

                shape = _geo.Rect.from_rect(
                    rect=via_m2bb, bottom=(toppin_bb.top + 2*m2.min_space),
                )
                layouter.add_wire(net=pin_net, wire=m2, shape=shape)
                _l = layouter.wire_layout(
                    net=pin_net, wire=via, rows=2,
                    bottom_enclosure="tall", top_enclosure="tall",
                )
                _via_m2bb = _l.bounds(mask=m2.mask)
                x = shape.right - _via_m2bb.left
                y = shape.bottom - _via_m2bb.bottom
                l = layouter.place(_l, x=x, y=y)
                via_m1bb = l.bounds(mask=m1.mask)

                shape = _geo.Rect.from_rect(rect=toppin_bb, top=via_m1bb.top)
                layouter.add_wire(net=pin_net, wire=m1, pin=m1pin, shape=shape)

            # connect sig_net
            if side in ("east", "west"):
                w = tech.on_grid(sigbuf_lipinbb.width, mult=2, rounding="floor")
                o_via = tech.on_grid(sigbuf_lipinbb.center)
                layouter.add_wire(
                    net=sig_net, wire=mcon, origin=o_via,
                    bottom_width=w, bottom_enclosure="wide",
                    top_width=w, top_enclosure="wide",
                )
                via_lay = layouter.add_wire(
                    net=sig_net, wire=via, origin=o_via,
                    bottom_width=w, bottom_enclosure="wide",
                    top_width=w, top_enclosure="wide",
                )
                via_m2bb = via_lay.bounds(mask=m2.mask)

                shape = _geo.Rect.from_rect(rect=sram_m2pinbb, bottom=via_m2bb.bottom)
                layouter.add_wire(net=sig_net, wire=m2, shape=shape)
                if side == "west":
                    shape = _geo.Rect.from_rect(rect=via_m2bb, right=sram_m2pinbb.right)
                else:
                    shape = _geo.Rect.from_rect(rect=via_m2bb, left=sram_m2pinbb.left)
                layouter.add_wire(net=sig_net, wire=m2, shape=shape)
            else:
                assert side == "south"

                h = tech.on_grid(sigbuf_lipinbb.height, mult=2, rounding="floor")
                o_via = tech.on_grid(sigbuf_lipinbb.center)
                layouter.add_wire(
                    net=sig_net, wire=mcon, origin=o_via,
                    bottom_height=h, bottom_enclosure="tall",
                    top_height=h, top_enclosure="tall",
                )
                via_lay = layouter.add_wire(
                    net=sig_net, wire=via, origin=o_via,
                    bottom_height=h, bottom_enclosure="tall",
                    top_height=h, top_enclosure="tall",
                )
                via_m2bb = via_lay.bounds(mask=m2.mask)

                if sigbuf_lipinbb.right < sram_m2pinbb.left:
                    bottom = dvdd_bottomrowm1bb.top + 2*(6 - idx)*conn_width
                    top = bottom + conn_width
                    shape = _geo.Polygon.from_floats(points=(
                        (via_m2bb.left, via_m2bb.bottom),
                        (via_m2bb.left, top),
                        (sram_m2pinbb.left, top),
                        (sram_m2pinbb.left, sram_m2pinbb.top),
                        (sram_m2pinbb.right, sram_m2pinbb.top),
                        (sram_m2pinbb.right, top - m2.min_width),
                        (sram_m2pinbb.left, top - m2.min_width),
                        (sram_m2pinbb.left, bottom),
                        (via_m2bb.right, bottom),
                        (via_m2bb.right, via_m2bb.bottom),
                        (via_m2bb.left, via_m2bb.bottom),
                    ))
                else:
                    bottom = dvdd_bottomrowm1bb.top + 2*(idx - 6)*conn_width
                    top = bottom + conn_width
                    shape = _geo.Polygon.from_floats(points=(
                        (via_m2bb.left, via_m2bb.bottom),
                        (via_m2bb.left, bottom),
                        (sram_m2pinbb.right, bottom),
                        (sram_m2pinbb.right, top - m2.min_width),
                        (sram_m2pinbb.left, top - m2.min_width),
                        (sram_m2pinbb.left, sram_m2pinbb.top),
                        (sram_m2pinbb.right, sram_m2pinbb.top),
                        (sram_m2pinbb.right, top),
                        (via_m2bb.right, top),
                        (via_m2bb.right, via_m2bb.bottom),
                        (via_m2bb.left, via_m2bb.bottom),
                    ))
                layouter.add_wire(net=sig_net, wire=m2, shape=shape)

        # connect the output signals
        for sig_name in (
            *(f"q[{bit}]" for bit in range(self.word_size)),
        ):
            side, idx = _find_sig(io_list=io_list, sig_name=sig_name)
            assert side != "north"
            io_net = io_nets[sig_name]
            io_type = io_list[side][idx][0]
            io_lay = io_lays[side][idx]

            assert io_type == "out"

            sram_port = sram.ports[sig_name]

            # instantiate cells
            buf = ckt.instantiate(buf_cell, name=f"buf_{sig_name}")

            # create nets
            pin_net = ckt.new_net(name=f"io_{sig_name}", external=True, childports=buf.ports.q)
            sig_net = ckt.new_net(name=sig_name, external=False, childports=(
                buf.ports.i, sram_port,
            ))

            # get bss
            toppin_bb = io_lay.bounds(mask=m1pin.mask, net=io_net, depth=1)
            sram_m2pinbb = sram_lay.bounds(mask=m2pin.mask, net=sig_net, depth=1)

            # place buf
            rot = rot_midrow
            y_row = y_midrow
            _buf_rotilipinbb = rot*_buf_ilipinbb
            x_buf = tech.on_grid(
                sram_m2pinbb.right - _buf_rotilipinbb.left,
                mult=2, rounding="ceiling",
            )
            buf_lay = layouter.place(buf, x=x_buf, y=y_row, rotation=rot)
            pinbuf_lipinbb = buf_lay.bounds(mask=lipin.mask, net=pin_net, depth=1)
            sigbuf_lipinbb = buf_lay.bounds(mask=lipin.mask, net=sig_net, depth=1)
            buf_bb = buf_lay.boundary
            assert buf_bb is not None

            # connect sig_net
            h = tech.on_grid(sigbuf_lipinbb.height, mult=2, rounding="floor")
            o_via = tech.on_grid(sigbuf_lipinbb.center)
            layouter.add_wire(
                net=sig_net, wire=mcon, origin=o_via,
                bottom_height=h, bottom_enclosure="tall",
                top_height=h, top_enclosure="tall",
            )
            via_lay = layouter.add_wire(
                net=sig_net, wire=via, origin=o_via,
                bottom_height=h, bottom_enclosure="tall",
                top_height=h, top_enclosure="tall",
            )
            via_m2bb = via_lay.bounds(mask=m2.mask)

            shape = _geo.Rect.from_rect(rect=sram_m2pinbb, bottom=via_m2bb.bottom)
            layouter.add_wire(net=sig_net, wire=m2, shape=shape)

            # connect pin_net
            h = tech.on_grid(pinbuf_lipinbb.height, mult=2, rounding="floor")
            o_via = tech.on_grid(pinbuf_lipinbb.center)

            layouter.add_wire(
                net=pin_net, wire=mcon, origin=o_via,
                bottom_height=h, bottom_enclosure="tall",
                top_height=h, top_enclosure="tall",
            )
            via_lay = layouter.add_wire(
                net=pin_net, wire=via, origin=o_via,
                bottom_height=h, bottom_enclosure="tall",
                top_height=h, top_enclosure="tall",
            )
            via_m2bb1 = via_lay.bounds(mask=m2.mask)

            if side in ("east", "west"):
                _l = layouter.wire_layout(
                    net=pin_net, wire=via, rows=2, columns=2,
                )
                _via_m2bb = _l.bounds(mask=m2.mask)
                x = bb.right - 2*m2.min_space - _via_m2bb.right
                y = toppin_bb.center.y
                
                l = layouter.place(_l, x=x, y=y)
                via_m1bb = l.bounds(mask=m1.mask)
                via_m2bb2 = l.bounds(mask=m2.mask)

                shape = _geo.Rect.from_rect(rect=toppin_bb, left=via_m1bb.left)
                layouter.add_wire(net=pin_net, wire=m1, pin=m1pin, shape=shape)
                shape = _geo.Rect.from_rect(rect=via_m2bb1, bottom=via_m2bb2.bottom)
                layouter.add_wire(net=pin_net, wire=m2, shape=shape)
                if side == "west":
                    shape = _geo.Rect.from_rect(rect=via_m2bb2, right=via_m2bb1.right)
                else:
                    shape = _geo.Rect.from_rect(rect=via_m2bb2, left=via_m2bb1.left)
                layouter.add_wire(net=pin_net, wire=m2, shape=shape)
            else:
                assert side == "south"

                _l = layouter.wire_layout(
                    net=pin_net, wire=via, rows=2, columns=2,
                )
                _via_m2bb = _l.bounds(mask=m2.mask)
                x = toppin_bb.center.x
                y = bb.bottom + 2*m2.min_space - _via_m2bb.bottom
                
                l = layouter.place(_l, x=x, y=y)
                via_m1bb = l.bounds(mask=m1.mask)
                via_m2bb = l.bounds(mask=m2.mask)

                shape = _geo.Rect.from_rect(rect=toppin_bb, top=via_m1bb.top)
                layouter.add_wire(net=pin_net, wire=m1, pin=m1pin, shape=shape)

                if via_m2bb.right < pinbuf_lipinbb.left:
                    bottom = dvdd_bottomrowm1bb.top + 2*(6 - idx)*conn_width
                    top = bottom + conn_width
                    shape = _geo.Polygon.from_floats(points=(
                        (via_m2bb.left, via_m2bb.bottom),
                        (via_m2bb.left, top),
                        (pinbuf_lipinbb.left, top),
                        (pinbuf_lipinbb.left, pinbuf_lipinbb.top),
                        (pinbuf_lipinbb.right, pinbuf_lipinbb.top),
                        (pinbuf_lipinbb.right, top - m2.min_width),
                        (pinbuf_lipinbb.left, top - m2.min_width),
                        (pinbuf_lipinbb.left, bottom),
                        (via_m2bb.right, bottom),
                        (via_m2bb.right, via_m2bb.bottom),
                        (via_m2bb.left, via_m2bb.bottom),
                    ))
                else:
                    bottom = dvdd_bottomrowm1bb.top + 2*(idx - 6)*conn_width
                    top = bottom + conn_width
                    shape = _geo.Polygon.from_floats(points=(
                        (via_m2bb.left, via_m2bb.bottom),
                        (via_m2bb.left, bottom),
                        (pinbuf_lipinbb.right, bottom),
                        (pinbuf_lipinbb.right, top - m2.min_width),
                        (pinbuf_lipinbb.left, top - m2.min_width),
                        (pinbuf_lipinbb.left, pinbuf_lipinbb.top),
                        (pinbuf_lipinbb.right, pinbuf_lipinbb.top),
                        (pinbuf_lipinbb.right, top),
                        (via_m2bb.right, top),
                        (via_m2bb.right, via_m2bb.bottom),
                        (via_m2bb.left, via_m2bb.bottom),
                    ))
                layouter.add_wire(net=pin_net, wire=m2, shape=shape)

        #
        # Connect the IO test signals
        m1conn_width = 2*m1.min_width
        m1conn_space = 2*m1.min_space
        m1conn_pitch = m1conn_width + m1conn_space

        bottom1 = bb.top - m1conn_pitch
        top1 = bottom1 + m1conn_width
        bottom2 = bottom1 - m1conn_pitch
        top2 = bottom2 + m1conn_width

        # Get layout of the two inout IO cells
        # lookup needs to happen on in signal
        side, idx = _find_sig(io_list=io_list, sig_name="inout_in")
        assert side == "north"
        inout_lay = io_lays[side][idx]
        side, idx = _find_sig(io_list=io_list, sig_name="inout2_in")
        assert side == "north"
        inout2_lay = io_lays[side][idx]

        # First in + out
        iosig_name1 = "in_in"
        io_net1 = io_nets[iosig_name1]
        side1, idx1 = _find_sig(io_list=io_list, sig_name=iosig_name1)
        io_lay1 = io_lays[side1][idx1]
        toppin_bb1 = io_lay1.bounds(mask=m1pin.mask, net=io_net1, depth=1)

        iosig_name2 = "in_in_direct"
        io_net2 = io_nets[iosig_name2]
        side2, idx2 = _find_sig(io_list=io_list, sig_name=iosig_name2)
        io_lay2 = io_lays[side2][idx2]
        toppin_bb2 = io_lay2.bounds(mask=m1pin.mask, net=io_net2, depth=1)

        iosig_name3 = "out_out"
        io_net3 = io_nets[iosig_name3]
        side3, idx3 = _find_sig(io_list=io_list, sig_name=iosig_name3)
        io_lay3 = io_lays[side3][idx3]
        toppin_bb3 = io_lay3.bounds(mask=m1pin.mask, net=io_net3, depth=1)

        iosig_name4 = "inout_out"
        io_net4 = io_nets[iosig_name4]
        toppin_bb4 = inout_lay.bounds(mask=m1pin.mask, net=io_net4, depth=1)

        assert (side1 == "north") and (side2 == "north") and (side3 == "north")
        assert toppin_bb1.right < toppin_bb2.left < toppin_bb3.left < toppin_bb4.left

        net = ckt.new_net(name=iosig_name1, external=True)

        shape = _geo.Rect.from_rect(rect=toppin_bb1, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, pin=m1pin, shape=shape)
        shape = _geo.Rect.from_rect(rect=toppin_bb2, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, shape=shape)
        shape = _geo.Rect.from_rect(rect=toppin_bb3, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, shape=shape)
        shape = _geo.Rect.from_rect(rect=toppin_bb4, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, shape=shape)
        shape = _geo.Rect(
            left=toppin_bb1.left, bottom=bottom1, right=toppin_bb4.right, top=top1,
        )
        layouter.add_wire(net=net, wire=m1, shape=shape)

        # First inout in connection
        iosig_name1 = "inout_in"
        io_net1 = io_nets[iosig_name1]
        toppin_bb1 = inout_lay.bounds(mask=m1pin.mask, net=io_net1, depth=1)

        iosig_name2 = "inout_in_direct"
        io_net2 = io_nets[iosig_name2]
        side2, idx2 = _find_sig(io_list=io_list, sig_name=iosig_name2)
        io_lay2 = io_lays[side2][idx2]
        toppin_bb2 = io_lay2.bounds(mask=m1pin.mask, net=io_net2, depth=1)

        assert side2 == "north"

        shape = _geo.Rect.from_rect(rect=toppin_bb1, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, pin=m1pin, shape=shape)
        shape = _geo.Rect.from_rect(rect=toppin_bb2, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, shape=shape)
        shape = _geo.Rect(
            left=toppin_bb1.left, bottom=bottom1, right=toppin_bb2.right, top=top1,
        )
        layouter.add_wire(net=net, wire=m1, shape=shape)

        # First inout outen connection
        iosig_name1 = "inout_outen"
        io_net1 = io_nets[iosig_name1]
        toppin_bb1 = inout_lay.bounds(mask=m1pin.mask, net=io_net1, depth=1)

        iosig_name2 = "in2_inout_outen"
        io_net2 = io_nets[iosig_name2]
        side2, idx2 = _find_sig(io_list=io_list, sig_name=iosig_name2)
        io_lay2 = io_lays[side2][idx2]
        toppin_bb2 = io_lay2.bounds(mask=m1pin.mask, net=io_net2, depth=1)

        assert side2 == "north"

        shape = _geo.Rect.from_rect(rect=toppin_bb1, bottom=bottom2)
        layouter.add_wire(net=net, wire=m1, pin=m1pin, shape=shape)
        shape = _geo.Rect.from_rect(rect=toppin_bb2, bottom=bottom2)
        layouter.add_wire(net=net, wire=m1, shape=shape)
        shape = _geo.Rect(
            left=toppin_bb1.left, bottom=bottom2, right=toppin_bb2.right, top=top2,
        )
        layouter.add_wire(net=net, wire=m1, shape=shape)

        # Second out
        iosig_name1 = "out2_out"
        io_net1 = io_nets[iosig_name1]
        side1, idx1 = _find_sig(io_list=io_list, sig_name=iosig_name1)
        io_lay1 = io_lays[side1][idx1]
        toppin_bb1 = io_lay1.bounds(mask=m1pin.mask, net=io_net1, depth=1)

        iosig_name2 = "out2_out_direct"
        io_net2 = io_nets["res_" + iosig_name2] # Connect to respad pin
        side2, idx2 = _find_sig(io_list=io_list, sig_name=iosig_name2)
        io_lay2 = io_lays[side2][idx2]
        toppin_bb2 = io_lay2.bounds(mask=m1pin.mask, net=io_net2, depth=1)

        assert (side1 == "north") and (side2 == "north")
        assert toppin_bb1.right < toppin_bb2.left

        net = ckt.new_net(name=iosig_name1, external=True)

        shape = _geo.Rect.from_rect(rect=toppin_bb1, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, pin=m1pin, shape=shape)
        shape = _geo.Rect.from_rect(rect=toppin_bb2, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, shape=shape)
        shape = _geo.Rect(
            left=toppin_bb1.left, bottom=bottom1, right=toppin_bb2.right, top=top1,
        )
        layouter.add_wire(net=net, wire=m1, shape=shape)

        # Second inout out signal
        iosig_name1 = "inout2_out"
        io_net1 = io_nets[iosig_name1]
        toppin_bb1 = inout2_lay.bounds(mask=m1pin.mask, net=io_net1, depth=1)

        iosig_name2 = "inout2_out_direct"
        io_net2 = io_nets[iosig_name2]
        side2, idx2 = _find_sig(io_list=io_list, sig_name=iosig_name2)
        io_lay2 = io_lays[side2][idx2]
        toppin_bb2 = io_lay2.bounds(mask=m1pin.mask, net=io_net2, depth=1)

        assert side2 == "north"
        assert toppin_bb2.right < toppin_bb1.left

        shape = _geo.Rect.from_rect(rect=toppin_bb1, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, pin=m1pin, shape=shape)
        shape = _geo.Rect.from_rect(rect=toppin_bb2, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, shape=shape)
        shape = _geo.Rect(
            left=toppin_bb2.left, bottom=bottom1, right=toppin_bb1.right, top=top1,
        )
        layouter.add_wire(net=net, wire=m1, shape=shape)

        # Second inout in signal
        iosig_name1 = "inout2_in"
        io_net1 = io_nets[iosig_name1]
        toppin_bb1 = inout2_lay.bounds(mask=m1pin.mask, net=io_net1, depth=1)

        iosig_name2 = "inout2_in_direct"
        io_net2 = io_nets[iosig_name2]
        side2, idx2 = _find_sig(io_list=io_list, sig_name=iosig_name2)
        io_lay2 = io_lays[side2][idx2]
        toppin_bb2 = io_lay2.bounds(mask=m1pin.mask, net=io_net2, depth=1)

        assert side2 == "north"
        assert toppin_bb1.right < toppin_bb2.left

        shape = _geo.Rect.from_rect(rect=toppin_bb1, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, pin=m1pin, shape=shape)
        shape = _geo.Rect.from_rect(rect=toppin_bb2, bottom=bottom1)
        layouter.add_wire(net=net, wire=m1, shape=shape)
        shape = _geo.Rect(
            left=toppin_bb1.left, bottom=bottom1, right=toppin_bb2.right, top=top1,
        )
        layouter.add_wire(net=net, wire=m1, shape=shape)

        # Second inout outen signal
        iosig_name1 = "inout2_outen"
        io_net1 = io_nets[iosig_name1]
        toppin_bb1 = inout2_lay.bounds(mask=m1pin.mask, net=io_net1, depth=1)

        iosig_name2 = "inout2_outen_direct"
        io_net2 = io_nets[iosig_name2]
        side2, idx2 = _find_sig(io_list=io_list, sig_name=iosig_name2)
        io_lay2 = io_lays[side2][idx2]
        toppin_bb2 = io_lay2.bounds(mask=m1pin.mask, net=io_net2, depth=1)

        assert side2 == "north"
        assert toppin_bb1.right < toppin_bb2.left

        shape = _geo.Rect.from_rect(rect=toppin_bb1, bottom=bottom2)
        layouter.add_wire(net=net, wire=m1, pin=m1pin, shape=shape)
        shape = _geo.Rect.from_rect(rect=toppin_bb2, bottom=bottom2)
        layouter.add_wire(net=net, wire=m1, shape=shape)
        shape = _geo.Rect(
            left=toppin_bb1.left, bottom=bottom2, right=toppin_bb2.right, top=top2,
        )
        layouter.add_wire(net=net, wire=m1, shape=shape)

        # boundary
        #
        layout.boundary = bb
        layouter.add_portless(prim=bnd, shape=bb)
