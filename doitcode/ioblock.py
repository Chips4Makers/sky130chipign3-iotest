# SPDX-License-Identifier: GPL-2.0-or-later
from math import floor
from typing import Tuple, Dict, Union, cast

from pdkmaster.technology import geometry as _geo, primitive as _prm
from pdkmaster.design import circuit as _ckt, layout as _lay, library as _lbry

from c4m.pdk import sky130

from . import core as _core


__all__ = ["IOBlock"]


class IOBlock(_lbry._Cell[_lbry.Library]):
    ring_bb = _geo.Rect(left=15.0, bottom=15.0, right=2905.0, top=3505.0)

    def __init__(self, *, lib: _lbry.Library):
        super().__init__(lib=lib, name="IOBlock")

        iofab = sky130.Sky130IOFactory(lib=self.lib)
        spec = iofab.spec

        tech = sky130.tech
        prims = tech.primitives

        m5 = cast(_prm.MetalWire, prims.m5)
        assert m5.pin is not None
        m5pin = m5.pin[0]
        pad = cast(_prm.PadOpening, prims.pad)
        bnd = cast(_prm.Marker, prims.prBoundary)

        ckt = self.new_circuit()
        nets = ckt.nets

        layouter = self.new_circuitlayouter()
        layout = layouter.layout

        # Get subcells
        io_cells: Dict[str, _lbry._Cell] = {
            "in": iofab.get_cell("IOPadIn"),
            "out": iofab.get_cell("IOPadOut"),
            "inout": iofab.get_cell("IOPadInOut"),
            "vdd": iofab.get_cell("IOPadVdd"),
            "vss": iofab.get_cell("IOPadVss"),
            "iovdd": iofab.get_cell("IOPadIOVdd"),
            "iovss": iofab.get_cell("IOPadIOVss"),
            "analog": iofab.get_cell("IOPadAnalog"),
        }
        corner_cell = iofab.get_cell("Corner")
        corner_bb = corner_cell.layout.boundary
        assert corner_bb is not None

        # Define io pad signals
        IOSpec = Tuple[str, Union[None, str, Tuple[str, str, str]]]
        io_list: Dict[str, Tuple[IOSpec, ...]] = {
            "north": ( # Left to right
                ("in", "in_in"),
                ("analog", "in_in_direct"),
                ("out", "out_out"),
                ("inout", ("inout_in", "inout_out", "inout_outen")),
                ("analog", "inout_in_direct"),
                ("in", "in2_inout_outen"),
                ("out", "out2_out"),
                ("analog", "out2_out_direct"),
                ("analog", "inout2_out_direct"),
                ("inout", ("inout2_in", "inout2_out", "inout2_outen")),
                ("analog", "inout2_in_direct"),
                ("analog", "inout2_outen_direct"),
            ),
            "east": ( # Bottom to top
                ("out", "q[4]"),
                ("in", "d[4]"),
                ("in", "d[5]"),
                ("out", "q[5]"),
                ("out", "q[6]"),
                ("in", "d[6]"),
                ("in", "d[7]"),
                ("out", "q[7]"),
                ("iovdd", None),
                ("iovss", None),
                ("vss", None),
                ("analog", "sram_vdd")
            ),
            "south": ( # Left to right
                ("in", "clk"),
                ("out", "q[0]"),
                ("in", "d[0]"),
                ("in", "d[1]"),
                ("in", "we[0]"),
                ("vss", None),
                ("vdd", None),
                ("out", "q[1]"),
                ("out", "q[2]"),
                ("in", "d[2]"),
                ("in", "d[3]"),
                ("out", "q[3]"),
            ),
            "west": ( # Bottom to top
                ("in", "a[0]"),
                ("in", "a[1]"),
                ("in", "a[2]"),
                ("in", "a[3]"),
                ("in", "a[4]"),
                ("in", "a[5]"),
                ("in", "a[6]"),
                ("in", "a[7]"),
                ("in", "a[8]"),
                ("iovdd", None),
                ("iovss", None),
                ("analog", "sram_vss")
            ),
        }

        # Create the DC nets
        vss = ckt.new_net(name="vss", external=True)
        vdd = ckt.new_net(name="vdd", external=True)
        iovss = ckt.new_net(name="iovss", external=True)
        iovdd = ckt.new_net(name="iovdd", external=True)

        # Instantiate the pad cells
        def instantiate_ios(
            iospecs: Tuple[IOSpec, ...], ioprefix: str,
        ) -> Tuple[_ckt._CellInstance, ...]:
            insts = []
            for i, (io_name, net_names) in enumerate(iospecs):
                inst = ckt.instantiate(io_cells[io_name], name=f"{ioprefix}{i + 1}")
                insts.append(inst)

                for dc_name in ("vss", "vdd", "iovss", "iovdd"):
                    nets[dc_name].childports += inst.ports[dc_name]

                if io_name in ("vss", "vdd", "iovss", "iovdd"):
                    assert net_names is None
                elif io_name in ("in", "out"):
                    assert isinstance(net_names, str)
                    inst_port = inst.ports.s if io_name == "in" else inst.ports.d
                    ckt.new_net(
                        name=net_names, external=False, childports=inst_port,
                    )
                    ckt.new_net(
                        name=("pad_" + net_names), external=True, childports=inst.ports.pad,
                    )
                elif io_name == "inout":
                    assert isinstance(net_names, tuple)
                    ckt.new_net(
                        name=net_names[0], external=False, childports=inst.ports.s,
                    )
                    ckt.new_net(
                        name=net_names[1], external=False, childports=inst.ports.d,
                    )
                    ckt.new_net(
                        name=net_names[2], external=False, childports=inst.ports.de,
                    )
                    ckt.new_net(
                        name=("pad_" + net_names[0]), external=True,
                        childports=inst.ports.pad,
                    )
                elif io_name == "analog":
                    assert isinstance(net_names, str)
                    ckt.new_net(
                        name=net_names, external=True, childports=inst.ports.pad,
                    )
                    ckt.new_net(
                        name=("res_" + net_names), external=False, childports=inst.ports.padres,
                    )
                else:
                    assert False, "Internal error"

            return tuple(insts)

        io_insts: Dict["str", Tuple[_ckt._CellInstance, ...]] = {
            side: instantiate_ios(iospecs=iospecs, ioprefix=f"IO_{side[0].upper()}")
            for side, iospecs in io_list.items()
        }

        #
        # Place the 4 corners
        inst = ckt.instantiate(corner_cell, name="C_SW")
        l = layouter.place(
            inst,
            x=(self.ring_bb.left - corner_bb.left),
            y=(self.ring_bb.bottom - corner_bb.bottom),
        )
        c_sw_bb = l.boundary
        assert c_sw_bb is not None
        inst = ckt.instantiate(corner_cell, name="C_SE")
        l = layouter.place(
            inst,
            x=(self.ring_bb.right + corner_bb.left),
            y=(self.ring_bb.bottom - corner_bb.bottom),
            rotation=_geo.Rotation.MY
        )
        c_se_bb = l.boundary
        assert c_se_bb is not None
        inst = ckt.instantiate(corner_cell, name="C_NW")
        l = layouter.place(
            inst,
            x=(self.ring_bb.left - corner_bb.left),
            y=(self.ring_bb.top + corner_bb.bottom),
            rotation=_geo.Rotation.MX
        )
        c_nw_bb = l.boundary
        assert c_nw_bb is not None
        inst = ckt.instantiate(corner_cell, name="C_NE")
        layouter.place(
            inst,
            x=(self.ring_bb.right + corner_bb.left),
            y=(self.ring_bb.top + corner_bb.bottom),
            rotation=_geo.Rotation.R180
        )

        # Helper for getting net for inst
        def get_net(*, side: str, idx: int):
            io_spec = io_list[side][idx]
            net_names = io_spec[1]
            if net_names is None:
                net_name = io_spec[0]
            elif isinstance(net_names, str):
                if io_spec[0] == "analog":
                    net_name = net_names
                else:
                    net_name = "pad_" + net_names
            else:
                assert isinstance(net_names, tuple)
                net_name = "pad_" + net_names[0]
            return ckt.nets[net_name]

        #
        # Place fillers and cells
        io_lays: Dict[str, Tuple[_lay._Layout, ...]] = {}

        # south of ring
        side = "south"
        cell_insts = io_insts[side]
        n_insts = len(cell_insts)
        cell_lays = []
        # Round length to 1um
        w_in = self.ring_bb.width - 2*corner_bb.width
        # filler on left and right half width of filler in between cells
        w_m_tot = floor(w_in/n_insts)*1.0
        w_m = w_m_tot - spec.iocell_width
        w_m_all = (n_insts - 1)*w_m + n_insts*spec.iocell_width
        w_l = floor(0.5*(w_in - w_m_all))*1.0
        w_r = w_in - w_l - w_m_all

        filler_l_cell = iofab.filler(cell_width=w_l)
        filler_m_cell = iofab.filler(cell_width=w_m)
        filler_r_cell = iofab.filler(cell_width=w_r)

        inst = ckt.instantiate(filler_l_cell, name="F_S_L")
        _filler_bb = filler_l_cell.layout.boundary
        assert _filler_bb is not None
        l = layouter.place(
            inst,
            x=(c_sw_bb.right - _filler_bb.left), y=(c_sw_bb.bottom - _filler_bb.bottom),
        )
        filler_bb = l.boundary
        assert filler_bb is not None

        for i, inst in enumerate(cell_insts):
            net = get_net(side=side, idx=i)

            _l = layouter.inst_layout(inst=inst)
            _cell_bb = _l.boundary
            assert _cell_bb is not None

            l = layouter.place(
                _l,
                x=(filler_bb.right - _cell_bb.left),
                y=(filler_bb.bottom - _cell_bb.bottom),
            )
            cell_lays.append(l)
            cell_bb = l.boundary
            assert cell_bb is not None

            # Make bond pad a pin
            m5pinbb = l.bounds(mask=pad.mask)
            layouter.add_wire(net=net, wire=m5, pin=m5pin, shape=m5pinbb)

            if i < (n_insts - 1):
                fill_inst = ckt.instantiate(filler_m_cell, name=f"F_S_M{i + 1}")
            else:
                fill_inst = ckt.instantiate(filler_r_cell, name=f"F_S_R")
            _l = layouter.inst_layout(inst=fill_inst)
            _filler_bb = _l.boundary
            assert _filler_bb is not None

            l = layouter.place(
                _l,
                x=(cell_bb.right - _filler_bb.left),
                y=(cell_bb.bottom - _filler_bb.bottom),
            )
            filler_bb = l.boundary
            assert filler_bb is not None
        io_lays[side] = tuple(cell_lays)

        # north of ring
        side = "north"
        cell_insts = io_insts[side]
        n_insts = len(cell_insts)
        cell_lays = []
        # Round length to 1um
        w_in = self.ring_bb.width - 2*corner_bb.width
        # filler on left and right half width of filler in between cells
        w_m_tot = floor(w_in/n_insts)*1.0
        w_m = w_m_tot - spec.iocell_width
        w_m_all = (n_insts - 1)*w_m + n_insts*spec.iocell_width
        w_l = floor(0.5*(w_in - w_m_all))*1.0
        w_r = w_in - w_l - w_m_all

        filler_l_cell = iofab.filler(cell_width=w_l)
        filler_m_cell = iofab.filler(cell_width=w_m)
        filler_r_cell = iofab.filler(cell_width=w_r)

        inst = ckt.instantiate(filler_l_cell, name="F_N_L")
        _l = layouter.inst_layout(inst=inst, rotation=_geo.Rotation.MX)
        _filler_bb = _l.boundary
        assert _filler_bb is not None
        l = layouter.place(
            _l,
            x=(c_nw_bb.right - _filler_bb.left), y=(c_nw_bb.top - _filler_bb.top),
        )
        filler_bb = l.boundary
        assert filler_bb is not None

        for i, inst in enumerate(cell_insts):
            net = get_net(side=side, idx=i)

            _l = layouter.inst_layout(inst=inst, rotation=_geo.Rotation.MX)
            _cell_bb = _l.boundary
            assert _cell_bb is not None

            l = layouter.place(
                _l,
                x=(filler_bb.right - _cell_bb.left),
                y=(filler_bb.top - _cell_bb.top),
            )
            cell_lays.append(l)
            cell_bb = l.boundary
            assert cell_bb is not None

            # Make bond pad a pin
            m5pinbb = l.bounds(mask=pad.mask)
            layouter.add_wire(net=net, wire=m5, pin=m5pin, shape=m5pinbb)

            if i < (n_insts - 1):
                fill_inst = ckt.instantiate(filler_m_cell, name=f"F_N_M{i + 1}")
            else:
                fill_inst = ckt.instantiate(filler_r_cell, name=f"F_N_R")
            _l = layouter.inst_layout(inst=fill_inst, rotation=_geo.Rotation.MX)
            _filler_bb = _l.boundary
            assert _filler_bb is not None

            l = layouter.place(
                _l,
                x=(cell_bb.right - _filler_bb.left),
                y=(cell_bb.top - _filler_bb.top),
            )
            filler_bb = l.boundary
            assert filler_bb is not None
        io_lays[side] = tuple(cell_lays)

        # west of ring
        side = "west"
        cell_insts = io_insts[side]
        n_insts = len(cell_insts)
        cell_lays = []
        # Round length to 1um
        w_in = self.ring_bb.height - 2*corner_bb.height
        # filler on left and right half width of filler in between cells
        w_m_tot = floor(w_in/n_insts)*1.0
        w_m = w_m_tot - spec.iocell_width
        w_m_all = (n_insts - 1)*w_m + n_insts*spec.iocell_width
        w_l = floor(0.5*(w_in - w_m_all))*1.0
        w_r = w_in - w_l - w_m_all

        filler_l_cell = iofab.filler(cell_width=w_l)
        filler_m_cell = iofab.filler(cell_width=w_m)
        filler_r_cell = iofab.filler(cell_width=w_r)

        inst = ckt.instantiate(filler_l_cell, name="F_W_L")
        _l = layouter.inst_layout(inst=inst, rotation=_geo.Rotation.R270)
        _filler_bb = _l.boundary
        assert _filler_bb is not None
        l = layouter.place(
            _l,
            x=(c_sw_bb.left - _filler_bb.left), y=(c_sw_bb.top - _filler_bb.bottom),
        )
        filler_bb = l.boundary
        assert filler_bb is not None

        for i, inst in enumerate(cell_insts):
            net = get_net(side=side, idx=i)

            _l = layouter.inst_layout(inst=inst, rotation=_geo.Rotation.R270)
            _cell_bb = _l.boundary
            assert _cell_bb is not None

            l = layouter.place(
                _l,
                x=(filler_bb.left - _cell_bb.left),
                y=(filler_bb.top - _cell_bb.bottom),
            )
            cell_lays.append(l)
            cell_bb = l.boundary
            assert cell_bb is not None

            # Make bond pad a pin
            m5pinbb = l.bounds(mask=pad.mask)
            layouter.add_wire(net=net, wire=m5, pin=m5pin, shape=m5pinbb)

            if i < (n_insts - 1):
                fill_inst = ckt.instantiate(filler_m_cell, name=f"F_W_M{i + 1}")
            else:
                fill_inst = ckt.instantiate(filler_r_cell, name=f"F_W_R")
            _l = layouter.inst_layout(inst=fill_inst, rotation=_geo.Rotation.R270)
            _filler_bb = _l.boundary
            assert _filler_bb is not None

            l = layouter.place(
                _l,
                x=(cell_bb.left - _filler_bb.left),
                y=(cell_bb.top - _filler_bb.bottom),
            )
            filler_bb = l.boundary
            assert filler_bb is not None
        io_lays[side] = tuple(cell_lays)

        # east of ring
        side = "east"
        cell_insts = io_insts[side]
        n_insts = len(cell_insts)
        cell_lays = []
        # Round length to 1um
        w_in = self.ring_bb.height - 2*corner_bb.height
        # filler on left and right half width of filler in between cells
        w_m_tot = floor(w_in/n_insts)*1.0
        w_m = w_m_tot - spec.iocell_width
        w_m_all = (n_insts - 1)*w_m + n_insts*spec.iocell_width
        w_l = floor(0.5*(w_in - w_m_all))*1.0
        w_r = w_in - w_l - w_m_all

        filler_l_cell = iofab.filler(cell_width=w_l)
        filler_m_cell = iofab.filler(cell_width=w_m)
        filler_r_cell = iofab.filler(cell_width=w_r)

        inst = ckt.instantiate(filler_l_cell, name="F_E_L")
        _l = layouter.inst_layout(inst=inst, rotation=_geo.Rotation.R90)
        _filler_bb = _l.boundary
        assert _filler_bb is not None
        l = layouter.place(
            _l,
            x=(c_se_bb.right - _filler_bb.right), y=(c_se_bb.top - _filler_bb.bottom),
        )
        filler_bb = l.boundary
        assert filler_bb is not None

        for i, inst in enumerate(cell_insts):
            net = get_net(side=side, idx=i)

            _l = layouter.inst_layout(inst=inst, rotation=_geo.Rotation.R90)
            _cell_bb = _l.boundary
            assert _cell_bb is not None

            l = layouter.place(
                _l,
                x=(filler_bb.right - _cell_bb.right),
                y=(filler_bb.top - _cell_bb.bottom),
            )
            cell_lays.append(l)
            cell_bb = l.boundary
            assert cell_bb is not None

            # Make bond pad a pin
            m5pinbb = l.bounds(mask=pad.mask)
            layouter.add_wire(net=net, wire=m5, pin=m5pin, shape=m5pinbb)

            if i < (n_insts - 1):
                fill_inst = ckt.instantiate(filler_m_cell, name=f"F_E_M{i + 1}")
            else:
                fill_inst = ckt.instantiate(filler_r_cell, name=f"F_E_R")
            _l = layouter.inst_layout(inst=fill_inst, rotation=_geo.Rotation.R90)
            _filler_bb = _l.boundary
            assert _filler_bb is not None

            l = layouter.place(
                _l,
                x=(cell_bb.right - _filler_bb.right),
                y=(cell_bb.top - _filler_bb.bottom),
            )
            filler_bb = l.boundary
            assert filler_bb is not None
        io_lays[side] = tuple(cell_lays)

        # Add SRAM subcell
        ring_inbb = _geo.Rect.from_rect(rect=self.ring_bb, bias=-spec.iocell_height)
        sram_cell = _core.Core(
            lib=lib, io_nets=nets, bb=ring_inbb, io_list=io_list, io_lays=io_lays,
        )
        inst = ckt.instantiate(sram_cell, name="sram")
        layouter.place(inst, origin=_geo.origin)

        # Boundary
        layout.boundary = self.ring_bb
        layouter.add_portless(prim=bnd, shape=self.ring_bb)
