# IO ring test vehicle

This is a project for generating an IO Ring made for wirebonding.
It is a follow-up to the [IOTestVehivle](https://platform.efabless.com/projects/621) tape-out on MPW4.

## Source

The top level is fully generated from python code in the `doitcode` subdirectory. pydoit is used to generate the desig with the provided `dodo.py` file in the top directory. The code depends on some external modules that are assumed to be installed:

* [PDKMaster](https://gitlab.com/Chips4Makers/PDKMaster): python framework (under heavy development) to ease generation of circuits and corresponding DRC compliant layout. This based on a description of a technology with python souce code to allow easy porting to
 different technologies.
* [c4m-flexcell](https://gitlab.com/Chips4Makers/c4m-flexcell): a (currently minimal) standard cell library based on PDKMaster
* [c4m-flexio](https://gitlab.com/Chips4Makers/c4m-flexio): the source for the IO pad cells used for this design
* [c4m-pdk-sky130](https://gitlab.com/Chips4Makers/c4m-pdk-sky130): the source for the PDKMaster based PDK for the Sky130 technology.

The state at tape-out of each of the repositories is given by the chipign3 branch.

## License

The resulting GDS files is released under [the LGPL 2.1 or later](https://spdx.org/licenses/LGPL-2.1-or-later.html) license. Some of the source code to generate the GDS is under [the GPL 2.0 or later](https://spdx.org/licenses/GPL-2.0-or-later.html) license.
